# -*- coding: utf-8 -*-
"""
#! -*- coding: utf-8 -*-
# 用CRF做中文命名实体识别
# 数据集 http://s3.bmio.net/kashgari/china-people-daily-ner-corpus.tar.gz
# 实测验证集的F1可以到96.18%，测试集的F1可以到95.35%
"""
import os
import sys
import numpy as np
import json
import logging
import tensorflow as tf
from tensorflow.python.keras.backend import set_session
from bert4keras.backend import keras, K
from bert4keras.models import build_transformer_model
from bert4keras.tokenizers import Tokenizer
from bert4keras.optimizers import Adam
from bert4keras.snippets import sequence_padding, DataGenerator
from bert4keras.snippets import open
from bert4keras.layers import ConditionalRandomField
from keras.layers import Dense
from keras.models import Model
from tqdm import tqdm

project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../"
))
sys.path.insert(0, project_dir)

from utils.logger_utils import setup_logging

setup_logging()

#
maxlen = 128
epochs = 20
batch_size = 32
# bert_layers = 6
learing_rate = 2e-5  # bert_layers越小，学习率应该要越大
crf_lr_multiplier = 1000  # 必要时扩大CRF层的学习率

# bert配置
config_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_small/albert_config.json"))
checkpoint_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_small/model.ckpt-best"
    ))
dict_path = os.path.normpath(os.path.join(
        project_dir, "data/albert_small/vocab_chinese.txt"
    ))
model_path = os.path.normpath(os.path.join(
    project_dir, "models/beauty_albert_small.weights"
))

# 默认配置
DEFAULT_KWARGS = {
    "pretrained_model_type": "albert",
    # "bert_layers": bert_layers,
    "config_path": config_path,
    "checkpoint_path": checkpoint_path,
    "dict_path": dict_path,
    "model_path": model_path,
    "labels": [
        '补水',
        '产品',
        '长痘痘',
        '成分',
        '防晒/隔离类',
        '肤质',
        '隔离',
        '黑头',
        '化妆水',
        '季节',
        '洁面类',
        '精华',
        '抗衰老',
        '抗氧化',
        '控油',
        '脸部部位',
        '毛孔',
        '美白亮肤',
        '面部部位',
        '面膜',
        '面霜',
        '年龄',
        '品牌',
        '其他问题',
        '祛斑',
        '祛痘',
        '去角质',
        '乳液',
        '色素斑',
        '深层清洁',
        '收敛',
        '舒缓抗敏',
        '细纹',
        '卸妆水',
        '修复',
        '眼部',
        '眼部护理',
        '原液',
        '孕期',
    ]
}


def _load_data(filename):
    """
    加载训练/验证/测试数据
    :param filename: 数据文件路径
    :return:
    """
    D = []
    with open(filename, encoding='utf-8') as f:
        f = f.read()
        for l in f.split('\n\n'):
            if not l:
                continue
            d, last_flag = [], ''
            for c in l.split('\n'):
                try:
                    char, this_flag = c.split(' ')
                except:
                    continue
                if this_flag == 'O' and last_flag == 'O':
                    d[-1][0] += char
                elif this_flag == 'O' and last_flag != 'O':
                    d.append([char, 'O'])
                elif this_flag[:1] == 'B':
                    d.append([char, this_flag[2:]])
                else:
                    d[-1][0] += char
                last_flag = this_flag
            D.append(d)
    return D


class NerDataGenerator(DataGenerator):
    """数据生成器
    """

    def __init__(self, data, tokenizer, label2id, batch_size):
        super(NerDataGenerator, self).__init__(data=data, batch_size=batch_size)
        self.tokenizer = tokenizer
        self.label2id = label2id

    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids, batch_labels = [], [], []
        for is_end, item in self.sample(random):
            token_ids, labels = [self.tokenizer._token_start_id], [0]
            for w, l in item:
                w_token_ids = self.tokenizer.encode(w)[0][1:-1]
                if len(token_ids) + len(w_token_ids) < maxlen:
                    token_ids += w_token_ids
                    if l == 'O':
                        labels += [0] * len(w_token_ids)
                    else:
                        B = self.label2id[l] * 2 + 1
                        I = self.label2id[l] * 2 + 2
                        labels += ([B] + [I] * (len(w_token_ids) - 1))
                else:
                    break
            token_ids += [self.tokenizer._token_end_id]
            labels += [0]
            segment_ids = [0] * len(token_ids)
            batch_token_ids.append(token_ids)
            batch_segment_ids.append(segment_ids)
            batch_labels.append(labels)
            if len(batch_token_ids) == self.batch_size or is_end:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
                batch_labels = sequence_padding(batch_labels)
                yield [batch_token_ids, batch_segment_ids], batch_labels
                batch_token_ids, batch_segment_ids, batch_labels = [], [], []


def _viterbi_decode(nodes, trans, num_labels):
    """Viterbi算法求最优路径
    其中nodes.shape=[seq_len, num_labels],
        trans.shape=[num_labels, num_labels].
    """
    labels = np.arange(num_labels).reshape((1, -1))
    scores = nodes[0].reshape((-1, 1))
    scores[1:] -= np.inf  # 第一个标签必然是0
    paths = labels
    for l in range(1, len(nodes)):
        M = scores + trans + nodes[l].reshape((1, -1))
        idxs = M.argmax(0)
        scores = M.max(0).reshape((-1, 1))
        paths = np.concatenate([paths[:, idxs], labels], 0)
    return paths[:, scores[:, 0].argmax()]


def _named_entity_recognize(text, the_model, the_crf, tokenizer, id2label):
    """命名实体识别函数
    """
    tokens = tokenizer.tokenize(text)
    while len(tokens) > 512:
        tokens.pop(-2)
    mapping = tokenizer.rematch(text, tokens)
    token_ids = tokenizer.tokens_to_ids(tokens)
    segment_ids = [0] * len(token_ids)
    nodes = the_model.predict([[token_ids], [segment_ids]])[0]
    trans = K.eval(the_crf.trans)
    labels = _viterbi_decode(nodes, trans, len(id2label)*2 + 1)
    # print(labels)
    entities, starting = [], False
    for i, label in enumerate(labels):
        if label > 0:
            if label % 2 == 1:
                starting = True
                entities.append([[i], id2label[(label - 1) // 2]])
            elif starting:
                entities[-1][0].append(i)
            else:
                starting = False
        else:
            starting = False

    return [
        (l, text[mapping[w[0]][0]:mapping[w[-1]][-1] + 1]) for w, l in entities
    ]


def _evaluate(data, the_model, the_crf, tokenizer, id2label):
    """评测函数
    """
    X, Y, Z = 1e-10, 1e-10, 1e-10
    for d in tqdm(data):
        try:
            text = ''.join([i[0] for i in d])
            R = set(_named_entity_recognize(text,
                                            the_model, the_crf,
                                            tokenizer, id2label))
            T = set([tuple(i[::-1]) for i in d if i[1] != 'O'])
            # print("\npredict:{}\ntrue:{}\n".format(R, T))
            X += len(R & T)
            Y += len(R)
            Z += len(T)
        except Exception as e:
            print(e.__str__())
    f1, precision, recall = 2 * X / (Y + Z), X / Y, X / Z
    return f1, precision, recall


class Evaluator(keras.callbacks.Callback):
    def __init__(self, model, crf,
                 tokenizer, id2label,
                 model_path,
                 dev_data, test_data):
        super(Evaluator, self).__init__()
        self.best_val_f1 = -1
        self.model = model
        self.crf = crf
        self.tokenizer = tokenizer
        self.id2label = id2label
        self.model_path = model_path
        self.dev_data = dev_data
        self.test_data = test_data

    def on_epoch_end(self, epoch, logs=None):
        f1, precision, recall = _evaluate(self.dev_data,
                                          self.model, self.crf,
                                          self.tokenizer, self.id2label)
        logging.info(
            '%s, 验证集:  f1: %.5f, precision: %.5f, recall: %.5f, best f1: %.5f\n' %
            (self.model_path, f1, precision, recall, self.best_val_f1)
        )
        # 保存最优
        if f1 >= self.best_val_f1:
            self.best_val_f1 = f1
            self.model.save_weights(self.model_path)
            logging.info("保存模型成功：{}".format(self.model_path))
        else:
            logging.info("未保存模型：{}".format(self.model_path))
        #
        f1, precision, recall = _evaluate(self.test_data,
                                          self.model, self.crf,
                                          self.tokenizer, self.id2label)
        logging.info(
            '%s, 测试集:  f1: %.5f, precision: %.5f, recall: %.5f\n' %
            (self.model_path, f1, precision, recall)
        )


class NerByModel:
    def __init__(self, kwargs=None):
        """
        :param kwargs:
                        'dict_path': 预训练模型的词汇表
                        'labels': 预测标签集合
                        'config_path': 预训练模型的配置文件
                        'pretrained_model_type': 预训练模型类型
                        'model_path': NER模型路径
                        --------以下参数只有在训练时才需要---------
                        'checkpoint_path': 预训练模型路径
                        'train_path': 训练数据文件路径
                        'dev_path': 验证数据文件路径
                        'test_path': 测试数据文件路径
        """
        if kwargs is None:
            kwargs = DEFAULT_KWARGS
        #
        dict_path = kwargs.get("dict_path")
        labels = kwargs.get("labels")
        self.config_path = kwargs.get("config_path")
        self.checkpoint_path = kwargs.get("checkpoint_path")
        self.pretrained_model_type = kwargs.get("pretrained_model_type")
        self.model_path = kwargs.get("model_path")
        self.train_path = kwargs.get("train_path")
        self.dev_path = kwargs.get("dev_path")
        self.test_path = kwargs.get("test_path")
        # 获取bert的隐藏层数
        with open(self.config_path, encoding="utf8")as fi:
            config = json.load(fi)
            self.bert_layers = config.get("num_hidden_layers")
        # 类别映射
        self.id2label = dict(enumerate(labels))
        self.label2id = {j: i for i, j in self.id2label.items()}
        self.num_labels = len(labels) * 2 + 1
        # 建立分词器
        self.tokenizer = Tokenizer(dict_path, do_lower_case=True)
        # 构建模型
        self.session = tf.Session()
        self.graph = tf.get_default_graph()
        set_session(self.session)
        self._create_model()
        if os.path.exists(self.model_path):
            self.load_model(self.model_path)
            print("加载模型成功：{}".format(self.model_path))
        else:
            print("加载模型失败：{}".format(self.model_path))

    def load_model(self, model_path):
        """
        加载训练好的判别模型
        :param model_path: 模型路径
        :return:
        """
        self.model.load_weights(model_path)

    def _create_model(self):
        """
        构建模型
        :return:
        """
        model = build_transformer_model(
            self.config_path,
            self.checkpoint_path,
            model=self.pretrained_model_type,
        )
        if "albert" == self.pretrained_model_type:
            output_layer = 'Transformer-FeedForward-Norm'
            output = model.get_layer(output_layer).get_output_at(self.bert_layers - 1)
        else:
            output_layer = 'Transformer-%s-FeedForward-Norm' % (self.bert_layers - 1)
            output = model.get_layer(output_layer).output
        output = Dense(self.num_labels)(output)
        self.crf_layer = ConditionalRandomField(lr_multiplier=crf_lr_multiplier)
        output = self.crf_layer(output)

        model = Model(model.input, output)
        model.summary()

        model.compile(
            loss=self.crf_layer.sparse_loss,
            optimizer=Adam(learing_rate),
            metrics=[self.crf_layer.sparse_accuracy]
        )
        self.model = model

    def train_and_test(self):
        """
        线下训练和测试模型
        :return:
        """
        train_data = _load_data(self.train_path)
        train_len = len(train_data)
        train_generator = NerDataGenerator(train_data, self.tokenizer, self.label2id, batch_size)
        del train_data
        evaluator = Evaluator(model=self.model,
                              crf=self.crf_layer,
                              tokenizer=self.tokenizer,
                              model_path=self.model_path,
                              id2label=self.id2label,
                              dev_data=_load_data(self.dev_path),
                              test_data=_load_data(self.test_path))
        self.model.fit_generator(
            train_generator.forfit(),
            steps_per_epoch=train_len // batch_size,
            epochs=epochs,
            callbacks=[evaluator]
        )

    def predict(self, text):
        """
        线上预测
        :param text: 输入文本
        :return:
        """
        with self.graph.as_default():
            set_session(self.session)
            ner_result = _named_entity_recognize(text,
                                                 self.model, self.crf_layer,
                                                 self.tokenizer, self.id2label)
        return ner_result


if __name__ == "__main__":
    import timeit
    #
    is_training = True
    #
    # 标注数据
    train_path = os.path.normpath(os.path.join(
        project_dir, 'data/beauty_robot/train.txt'))
    dev_path = os.path.normpath(os.path.join(
        project_dir, 'data/beauty_robot/dev.txt'))
    test_path = os.path.normpath(os.path.join(
        project_dir, 'data/beauty_robot/test.txt'))
    #
    if is_training:
        DEFAULT_KWARGS.update({
            "pretrained_model_type": "bert",
            "train_path": train_path,
            "dev_path": dev_path,
            "test_path": test_path,
            "config_path": os.path.normpath(os.path.join(
                project_dir, "data/chinese_L-12_H-768_A-12/bert_config.json")),
            "checkpoint_path": os.path.normpath(os.path.join(
                project_dir, "data/chinese_L-12_H-768_A-12/bert_model.ckpt")),
            "dict_path": os.path.normpath(os.path.join(
                project_dir, "data/chinese_L-12_H-768_A-12/vocab.txt")),
            "model_path": os.path.normpath(os.path.join(
                project_dir, "models/beauty_bert_20200722.weights")),
        })
    ner = NerByModel(DEFAULT_KWARGS)
    if is_training:
        ner.train_and_test()
    y = "y"
    while "y" == y:
        t = input("请输入：")
        ss = timeit.default_timer()
        result = ner.predict(t)
        ee = timeit.default_timer()
        print("结果: {}, 耗时: {:.5f}".format(result, ee - ss))
        y = input("继续/y, 退出/n ?")
