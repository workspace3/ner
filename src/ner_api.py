# -*- coding: utf-8 -*-
"""
NER接口
"""
import os
import sys

project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../"
))
sys.path.insert(0, project_dir)

from src.ner_by_model import NerByModel
from src.ner_by_dict import NerByDicts


class Ner:
    def __init__(self):
        # 采用模型的ner
        self.ner_by_model = NerByModel()
        # warm up
        _ = self.ner_by_model.predict("测试测试")
        # 采用词表的ner
        self.ner_by_dicts = NerByDicts()

    def predict(self, text, by="both"):
        """
        在线预测
        :param text: 输入文本
        :param by: 抽取方式（model, dicts, both）
        :return:
        """
        if "model" == by:
            return self.ner_by_model.predict(text)
        elif "dicts" == by:
            return self.ner_by_dicts.predict(text)
        else:
            return list(set(self.ner_by_model.predict(text) +
                            self.ner_by_dicts.predict(text)))


if __name__ == "__main__":
    import timeit
    ner = Ner()
    y = "y"
    while "y" == y:
        t = input("请输入：")
        ss = timeit.default_timer()
        result = ner.predict(t)
        ee = timeit.default_timer()
        print("结果: {}, 耗时: {:.5f}".format(result, ee - ss))
        y = input("继续/y, 退出/n ?")
