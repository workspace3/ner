# -*- coding: utf-8 -*-
"""
采用词表的NER方式
"""
import os
import sys
import ahocorasick

project_dir = os.path.normpath(os.path.join(
    os.path.dirname(__file__), "../"
))
sys.path.insert(0, project_dir)

# toy dicts
DEFAULT_DICTS = {
        "品牌": ["欧莱雅", "资生堂", "大宝"],
        "成分": ["甘油", "酒精"],
        "功效": ["保湿", "祛斑", "美白"]
}


class NerByDicts:
    def __init__(self, dicts=None):
        """
        初始化抽取器
        :param dicts:
                        key=实体名称, str
                        value=实体值, list
        """
        if dicts is None:
            dicts = DEFAULT_DICTS
        self.extractors = dict()
        for key, values in dicts.items():
            self.extractors[key] = ahocorasick.AhoCorasick(*values)

    def predict(self, text):
        """
        在线预测
        :param text: 输入文本
        :return:
        """
        entities = []
        for entity_name, extractor in self.extractors.items():
            entity_values = extractor.search(text)
            entities.extend([(entity_name, v) for v in entity_values])
        return entities


if __name__ == "__main__":
    import timeit
    dicts = {
        "品牌": ["欧莱雅", "资生堂", "大宝"],
        "成分": ["甘油", "酒精"],
        "功效": ["保湿", "祛斑", "美白"]
    }
    ner = NerByDicts(dicts)
    y = "y"
    while "y" == y:
        t = input("请输入：")
        ss = timeit.default_timer()
        result = ner.predict(t)
        ee = timeit.default_timer()
        print("结果: {}, 耗时: {:.5f}".format(result, ee - ss))
        y = input("继续/y, 退出/n ?")
