# _*_ coding: utf-8 _*-

import os
import logging
import traceback
from configparser import ConfigParser


logger = logging.getLogger(__name__)


class Config(object):
    def __init__(self, project_path, conf_path):
        self._project_path = project_path
        self._conf_path = os.path.join(self._project_path, conf_path)
        self.parser = ConfigParser()
        self.parser.read(self._conf_path)

    def get_value(self, section, key):
        try:
            res = str(self.parser.get(section, key))
            return res
        except Exception:
            logger.error(traceback.format_exc())

    def get_items(self, section):
        try:
            res = self.parser.items(section)
            return dict((x, y) for x, y in res)
        except Exception:
            logger.error(traceback.format_exc())

    def get_sections(self):
        try:
            res = self.parser.sections()
            return res
        except Exception:
            logger.error(traceback.format_exc())

    def get_options(self, section):
        try:
            res = self.parser.options(section)
            return res
        except Exception:
            logger.error(traceback.format_exc())


if __name__ == '__main__':
    pass
